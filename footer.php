<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Base-Theme
 */

?>

	</div><!-- #content -->

	<footer id="bottom" class="row col-container pink" role="contentinfo">
		<div class="col three-col">
			<?php if ( is_active_sidebar( 'footer-left-col' ) ) : ?>

				<?php dynamic_sidebar( 'footer-left-col' ); ?>

			<?php else : ?>

				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" id="logo-bottom"><img src="<?php bloginfo('template_url'); ?>/assets/images/src/logo.png" id="logo-bottom1"></a>

				<p>This website uses cookies to improve your user experience. We'll assume you're ok with that.</p>

			<?php endif; ?>
			
		</div>
		<div class="col three-col">
			<?php if ( is_active_sidebar( 'footer-middle-col' ) ) : ?>

				<?php dynamic_sidebar( 'footer-middle-col' ); ?>

				<?php else : ?>

					<h2>Telephone Number</h2>
					<p>telno</p>
					<h2>Email Address</h2>
					<p>emailaddress</p>

			<?php endif; ?>
		</div>
		<div class="col three-col">
			<?php if ( is_active_sidebar( 'footer-right-col' ) ) : ?>

				<?php dynamic_sidebar( 'footer-right-col' ); ?>

				<?php else : ?>
					
					<h2>Address</h2>
					<p>address</p>

			<?php endif; ?>
		</div>
	</footer><!-- #colophon -->
	<div id="end" class="row ">
		<div class="copyright">
			<p>2017 Copyright Base-Theme</p>
		</div>
		<div class="site-design">
			<p>Website Design by: XXXX</p>
		</div>
	</div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
