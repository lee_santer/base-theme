// require('es6-promise').polyfill();

// Defining base pathes
// var basePaths = {
//     bower: './bower_components/',
//     node: './node_modules/',
//     dev: './assets/'
// };

//BASE GULP
var gulp          = require('gulp');
//SASS
var sass          = require('gulp-sass');
var autoprefixer  = require('gulp-autoprefixer');
//RTL and RENAME
var rtlcss       = require('gulp-rtlcss');
var rename       = require('gulp-rename');
var plumber = require('gulp-plumber');
//ERORR LOGGING
var gutil = require('gulp-util');
//JS
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
//IMAGE OP.
var imagemin = require('gulp-imagemin');
//BROWSER-SYNC
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

var onError = function (err) {
  console.log('An error occurred:', gutil.colors.magenta(err.message));
  gutil.beep();
  this.emit('end');
};


//Sass Tasks
gulp.task('sass', function() {
  return gulp.src('./sass/**/*.scss')
  .pipe(plumber({ errorHandler: onError }))
  .pipe(sass())
  .pipe(autoprefixer())
  .pipe(gulp.dest('./'))

  .pipe(rtlcss())                     // Convert to RTL
  .pipe(rename({ basename: 'rtl' }))  // Rename to rtl.css
  .pipe(gulp.dest('./'));             // Output RTL stylesheets (rtl.css)
});


//JS Tasks
gulp.task('js', function() {
  return gulp.src(['./js/*.js', './js/navigation.js', './js/skip-link-focus-fix.js'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(concat('app.js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('./js'))
});

//IMAGE Tasks
gulp.task('images', function() {
  return gulp.src('.assets/images/src/*')
    .pipe(plumber({errorHandler: onError}))
    .pipe(imagemin({optimizationLevel: 7, progressive: true}))
    .pipe(gulp.dest('.assets/images/dist'));
});

//Gulp Tasks

//Gulp Watch
gulp.task('watch', function() {
  browserSync.init({
    files: ['./**/*.php'],
    proxy: 'http://localhost/base-theme/',
  });
  gulp.watch('./sass/**/*.scss', ['sass', reload]);
  gulp.watch('./js/*.js', ['js', reload]);
  gulp.watch('images/src/*', ['images', reload]);
});

//Gulp
gulp.task('default', ['sass', 'js', 'images', 'watch']);
