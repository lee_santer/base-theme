<?php
/**
 * Sample implementation of the Custom Header feature
 *
 * You can add an optional custom header image to header.php like so ...
 *
	<?php the_header_image_tag(); ?>
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-headers/
 *
 * @package Base-Theme
 */


//remove display notice - Showing all x results
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
//remove default sorting drop-down from WooCommerce
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );



function tutsplus_product_subcategories( $args = array() ) {
    $parentid = get_queried_object_id();
         
	$args = array(
	    'parent' => $parentid
	);
	 
	$terms = get_terms( 'product_cat', $args ); ?>
	 
	

	<div class="col-container" id="woocommerce_product-filtering">
		<div class="col two-thirds-col" id="woocommerce_cat">

			<?php
			
			echo '<ul class="product-cats">';

			    echo '<li class="category"><a href="localhost:3000/base-theme/shop">All</a></li>';
			     
			        foreach ( $terms as $term ) {
			                         
			            echo '<li class="category">';                 
			                 	echo ' / '; 
			                    echo '<a href="' .  esc_url( get_term_link( $term ) ) . '" class="' . $term->slug . '"> ';
			                        echo $term->name;

			                    echo '</a>';
			                                                                    
			            echo '</li>';
			    }
			     
			    echo '</ul>';
			 
 		?>
		</div>
		<div class="col one-third-col" id="woocommerce_filtering">
			<a href="#" id="woo-filter" class="drop-down-btn">Filter</a> / <a href="#" id="woo-search" class="drop-down-btn">Search</a>
		</div>
	
	</div>

	<div class="drop-down">
		<p>SHOW FILTERS</p>
	</div>
	<div class="drop-down search">
		<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
			<div class="col-container">
				<input type="text" name="s" id="s" placeholder="Type in keyword">
				<a href="#"><i class="icon-cancel drop-down-close"></i></a>
			</div>
		</form>
	</div>
	         
	    



   <?php }

add_action( 'woocommerce_before_shop_loop', 'tutsplus_product_subcategories', 50 );

