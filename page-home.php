<?php
/**
 * The front page template file.
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear. Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 *
 * @package Base-Theme
 */
/**
 * Load pattern maker file.
 */
?>

<?php get_template_part( 'template-parts/owl-slider', 'none' ); ?>

<section class="row grey col-container">
	<div class="col three-col">
		<p>Testing</p>
	</div>
	<div class="col three-col">
		<p>Testing</p>
	</div>
	<div class="col three-col">
		<p>Testing</p>
	</div>
</section>

<section class="row blue col-container">
	<div class="col three-col">
		<p>Testing</p>
	</div>
	<div class="col three-col">
		<p>Testing</p>
	</div>
	<div class="col three-col">
		<p>Testing</p>
	</div>
</section>
