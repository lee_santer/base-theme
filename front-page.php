<?php
/**
 * The front page template file.
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear. Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 *
 * @package Base-Theme
 */
/**
 * Load pattern maker file.
 */
get_header(); ?>
	<!-- Adds Page Template Option to Static Home Page -->
	<?php if ( 'posts' == get_option( 'show_on_front' ) ) {
    include( get_home_template() );
	} else {
	    include( get_page_template() );
	} ?>
<?php get_footer(); ?>