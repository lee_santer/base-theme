
<?php
/*
Template Name: WooCommerce Front Page
*/
?>

<?php get_template_part( 'template-parts/owl-slider', 'none' ); ?>

<section class="row grey">
	<?php echo do_shortcode('[recent_products per_page="4" columns="4"]'); ?>
</section>