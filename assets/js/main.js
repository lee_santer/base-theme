$(document).ready(function() {
	$(window).scroll(function() {
		var scroll = $(window).scrollTop();

		if (scroll >= 300) {
			$('body').addClass('scrolled');
		} else {
			$('body').removeClass('scrolled');
		}
	});


	var nav = $('body');
	var scrolled = false;

	$(window).scroll(function () {
	if (630 < $(window).scrollTop() && !scrolled) {
	    nav.addClass('fixed'); 
	    $('.navbar').animate({top: 0}, 1000);
	    scrolled = true;
	  }

	  if (630 > $(window).scrollTop() && scrolled) {
	    $('.navbar').animate({top: -80}, 200, function() {
	        nav.removeClass('fixed');
	        $('.navbar').removeAttr('style');
	    });
	    scrolled = false;
	  }
	}); 

	 $('.owl-carousel').owlCarousel({
	    loop:true,
	    items:1,
	    nav: true,
	    navText: [","],
	    // navContainer: '.wrapper',
	    autoHeight: false,
	    dots: false
	});
	// $('.single-item').slick();

	// $('#woo-filter').click(function() {
 //            $('#drop-down-filter').slideToggle("fast");
 //            $('#drop-down-search').slideUp("fast");
 //    });
 //    $('#woo-search').click(function() {
 //            $('#drop-down-search').slideToggle("fast");
 //            $('#drop-down-filter').slideUp("fast");

 //    });

    $('.drop-down-btn').on('click', function(){
    var self = $('.drop-down').eq($(this).index()),
        vis  = $('.drop-down').not(self).filter(':visible');
    
    if (vis.length) {
        vis.slideUp(function() {
            self.slideToggle();
        });
    }else{
        self.slideToggle();        
    }

    $('.drop-down-close').on('click', function(){
    	$('.drop-down').slideUp();
    });

});

});