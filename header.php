<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Base-Theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link href="https://file.myfontastic.com/vqDDRMByRTVxPmuGR9djWL/icons.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:400,700|Open+Sans:400,600,700" rel="stylesheet">
<!-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/> -->
<!-- <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script> -->
				

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	

	<div id="top" class="row top-container">
		<div class="social">
		<a class="icon-twitter" href="http://twitter.com/jason_bradley" title="Twitter" target="_blank"></a>
		<a class="icon-facebook" href="http://twitter.com/jason_bradley" title="Twitter" target="_blank"></a>
		<a class="icon-google-plus" href="http://twitter.com/jason_bradley" title="Twitter" target="_blank"></a>
		<a class="icon-instagram" href="http://twitter.com/jason_bradley" title="Twitter" target="_blank"></a>
		<a class="icon-envelope" href="http://twitter.com/jason_bradley" title="Twitter" target="_blank"></a>
		<a class="icon-phone" href="http://twitter.com/jason_bradley" title="Twitter" target="_blank"></a>
		</div>
		<div class="contact">
		<a href="mailto:email@base-theme.com">email@base-theme.com</a><a href="telno:+44 (0)1234 567 890">+44 (0)1234 567 890</a>
		</div>
	</div>

	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'base-theme' ); ?></a>
	
	<header id="header" class="row grey" role="header">
		<div class="site-branding">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" id="logo"><img src="<?php bloginfo('template_url'); ?>/assets/images/src/logo.png" ></a>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation navbar" role="navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><span class="icon-bars"></span></button>
			<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content">
 